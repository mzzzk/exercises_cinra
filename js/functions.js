$(function() {

/* ----------------
	初期設定
 ---------------- */
	var canvas = $('#canvas').get(0);

	if (!canvas || !canvas.getContext) return false;
	var ctx = canvas.getContext('2d');

	var	_window_w,
		_window_h,
		_container = $(".container"),
		_container__box = $(".container__box");	

/* ----------------
	config
 ---------------- */
	//斜めラインの設定
	var _lineColor = "#f00",
		_lineWidth = 1;
	
	
/* ----------------
	線の描画
----------------- */
	function drawLine() {

		_window_w = window.innerWidth ? window.innerWidth: $(window).width();
		_window_h = window.innerHeight ? window.innerHeight: $(window).height();

		ctx.beginPath();
		
		//canvas・containerのサイズをwindowサイズに
		canvas.width = _window_w;
		canvas.height = _window_h;
		_container.css({
			width: _window_w + "px",
			height: _window_h + "px"
		});

		//ラインの設定
		ctx.strokeStyle = _lineColor;
		ctx.lineWidth = _lineWidth;
		
		//ラインの描画
		ctx.moveTo(0, 0);
		ctx.lineTo(_window_w, _window_h);
		ctx.stroke();
				
	}

/* ----------------
	白背景を生成
----------------- */
	_container__box.prepend('<div class="js-container__box__bg" />');

	var _boxHeight;
	function bgCreate() {
		_boxHeight = $(".container__box__inner").height();
		$(".js-container__box__bg").css({
			height: _boxHeight + "px"
		});
	}
	

/* ----------------
	ページ表示時とリサイズ時に実行
----------------- */
	$(window).on('load resize', function(){
		drawLine();
		bgCreate();
	});
	
});